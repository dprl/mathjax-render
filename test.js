#! /usr/bin/env node

/*************************************************************************
 *
 *  direct/tex2svg
 *
 *  Uses MathJax v3 to convert a TeX string to an SVG string.
 *
 * ----------------------------------------------------------------------
 *
 *  Copyright (c) 2018 The MathJax Consortium
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

//
//  Load the packages needed for MathJax
//
const {mathjax} = require('mathjax-full/js/mathjax.js');
const {TeX} = require('mathjax-full/js/input/tex.js');
const {SVG} = require('mathjax-full/js/output/svg.js');
const {liteAdaptor} = require('mathjax-full/js/adaptors/liteAdaptor.js');
const {RegisterHTMLHandler} = require('mathjax-full/js/handlers/html.js');
const {AssistiveMmlHandler} = require('mathjax-full/js/a11y/assistive-mml.js');

const {AllPackages} = require('mathjax-full/js/input/tex/AllPackages.js');

//
//  Get the command-line arguments
//
var argv = require('yargs')
    .demand(0).strict()
    .usage('$0 [options] "math" > file.svg')
    .options({
        inline: {
            boolean: true,
            describe: "process as inline math"
        },
        em: {
            default: 16,
            describe: 'em-size in pixels'
        },
        ex: {
            default: 8,
            describe: 'ex-size in pixels'
        },
        width: {
            default: 80 * 16,
            describe: 'width of container in pixels'
        },
        packages: {
            default: AllPackages.sort().join(', '),
            describe: 'the packages to use, e.g. "base, ams"'
        },
        css: {
            boolean: true,
            describe: 'output the required CSS rather than the SVG itself'
        },
        fontCache: {
            boolean: true,
            default: true,
            describe: 'whether to use a local font cache or not'
        },
        assistiveMml: {
            boolean: true,
            default: false,
            describe: 'whether to include assistive MathML output'
        }
    })
    .argv;

//
//  Create DOM adaptor and register it for HTML documents
//
const adaptor = liteAdaptor();
const handler = RegisterHTMLHandler(adaptor);
if (argv.assistiveMml) AssistiveMmlHandler(handler);

//
//  Create input and output jax and a document using them on the content from the HTML file
//
const tex = new TeX({packages:AllPackages,
                      macros: {
                        B: "{\\mathbb{B}}",
                        C: "{\\mathbb{C}}",
                        K: "{\\mathbb{K}}",
                        R: "{\\mathbb{R}}",
                        Q: "{\\mathbb{Q}}",
                        Z: "{\\mathbb{Z}}",
                        Alpha: "{A}",
                        Eta: "{E}",
                        Epsilon: "{E}",
                        Iota: "{\\iota}",
                        Mu: "{\\mathrm{M}}",
                        Nu: "{\\mathrm{N}",
                        Omicron: "{\\mathrm{O}}",
                        omicron: "{\\mathrm{o}}",
                        PI: "{\\Pi}",
                        Rho: "{\\rho}",
                        Tau: "{\\tau}",
                        Chi: "X",
                        real: "{\\mathbb{R}}",
                        reals: "{\\mathbb{R}}",
                        Complex: "{\\mathbb{C}}",
                        Zeta: "{\\mathbb{Z}}",
                        alef: "{\\aleph}",
                        ang: "{\\angle}",
                        bull: "{\\bullet}",
                        darr: "{\\downarrow}",
                        Dagger: "{\\dagger}",
                        exist: "{{\\exists}",
                        isin: "{\\in}",
                        Larr: "{\\Leftarrow}",
                        lrarr: "{\\leftrightarrow}",
                        plusmn: "{\\pm}",
                        Rarr: "{\\Rightarrow}",
                        sdot: "{\\cdot}",
                        sub: "{\\subset}",
                        sube: "{\\subseteq}",
                        supe: "{\\supseteq}",
                        uarr: "{\\uparrow}",
                        lt: "{<}",
                        gt: "{<}",
                        Cos: "{\\cos}",
                        Sin: "{\\sin}",
                        Tan: "{\\tan}",
                        arcsec: "{arcsec}",
                        arccot: "{arccot}",
                        arccsc: "{arccsc}"
                         }}) 
const svg = new SVG({fontCache: 'none'});

const html = mathjax.document('', {InputJax: tex, OutputJax: svg});

//
//  Typeset the math from the command line
//
const node = html.convert(argv._[0] || '', {
    display: !argv.inline,
    em: argv.em,
    ex: argv.ex,
    containerWidth: argv.width
});

//
//  If the --css option was specified, output the CSS,
//  Otherwise, typeset the math and output the HTML
//
if (argv.css) {
    console.log(adaptor.textContent(svg.styleSheet(html)));
} else {
    console.log(adaptor.outerHTML(node));
}
