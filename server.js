var express = require("express");
var cors = require("cors");
const jsdom = require("jsdom");
const { JSDOM } = jsdom;

var errorHandler = require("errorhandler");
var bodyParser = require("body-parser");
var jsonParser = bodyParser.json();
var app = express();

app.use(errorHandler({ dumpExceptions: true, showStack: true }));

const { mathjax } = require("mathjax-full/js/mathjax.js");
const { TeX } = require("mathjax-full/js/input/tex.js");
const { SVG } = require("mathjax-full/js/output/svg.js");
const { MathML } = require("mathjax-full/js/input/mathml.js");

const { liteAdaptor } = require("mathjax-full/js/adaptors/liteAdaptor.js");
const { jsdomAdaptor } = require("mathjax-full/js/adaptors/jsdomAdaptor.js");
const { RegisterHTMLHandler } = require("mathjax-full/js/handlers/html.js");

const { AllPackages } = require("mathjax-full/js/input/tex/AllPackages.js");
const swaggerUi = require("swagger-ui-express");
const swaggerDocument = require("./swagger.json");
//
//  Create DOM adaptor and register it for HTML documents
//
const {
  HTMLDocument,
} = require("mathjax-full/js/handlers/html/HTMLDocument.js");
const { use } = require("express/lib/application");

var options = {
  explorer: true,
};
app.use(
  "/api-docs",
  swaggerUi.serve,
  swaggerUi.setup(swaggerDocument, options)
);
app.use(cors());

const adaptor = jsdomAdaptor(JSDOM);
const handler = RegisterHTMLHandler(adaptor);

app.post("/render/", jsonParser, function (req, res) {
  const svg = new SVG({ fontCache: "none" });

  const tex = new TeX({
    packages: AllPackages,
    macros: {
      B: "{\\mathbb{B}}",
      C: "{\\mathbb{C}}",
      K: "{\\mathbb{K}}",
      R: "{\\mathbb{R}}",
      Q: "{\\mathbb{Q}}",
      Z: "{\\mathbb{Z}}",
      Alpha: "{A}",
      Eta: "{E}",
      Epsilon: "{E}",
      Iota: "{\\iota}",
      Mu: "{\\mathrm{M}}",
      Nu: "{\\mathrm{N}",
      Omicron: "{\\mathrm{O}}",
      omicron: "{\\mathrm{o}}",
      PI: "{\\Pi}",
      Rho: "{\\rho}",
      Tau: "{\\tau}",
      Chi: "X",
      real: "{\\mathbb{R}}",
      reals: "{\\mathbb{R}}",
      Complex: "{\\mathbb{C}}",
      Zeta: "{\\mathbb{Z}}",
      alef: "{\\aleph}",
      ang: "{\\angle}",
      bull: "{\\bullet}",
      darr: "{\\downarrow}",
      Dagger: "{\\dagger}",
      exist: "{{\\exists}",
      isin: "{\\in}",
      Larr: "{\\Leftarrow}",
      lrarr: "{\\leftrightarrow}",
      plusmn: "{\\pm}",
      Rarr: "{\\Rightarrow}",
      sdot: "{\\cdot}",
      sub: "{\\subset}",
      sube: "{\\subseteq}",
      supe: "{\\supseteq}",
      uarr: "{\\uparrow}",
      lt: "{<}",
      gt: "{<}",
      Cos: "{\\cos}",
      Sin: "{\\sin}",
      Tan: "{\\tan}",
      arcsec: "{arcsec}",
      arccot: "{arccot}",
      arccsc: "{arccsc}",
    },
  });
  var html = mathjax.document("", {
    InputJax: tex,
    OutputJax: svg,
  });
  try {
    var node = html.convert(req.body.latex);
    const str = adaptor.outerHTML(node);
    adaptor.document.body.innerHTML = "";
    res.send(str);
  } catch (err) {
    res.status(400).json({ error: err.toString() });
  }
});

// render mathml as svg
app.post("/render-mathml/", jsonParser, function (req, res) {
  const mathml = new MathML();
  const svg = new SVG({ fontCache: "none" });
  let m = req.body.mathml.replace(/\n/g, "").replace(/\\n/g, "");
  const html = mathjax.document("", {
    InputJax: mathml,
    OutputJax: svg,
  });
  var node = html.convert(m);
  const dom = new JSDOM(adaptor.outerHTML(node));
  dom.window.document
    .getElementsByTagName("svg")[0]
    .setAttribute("width", "100%");
  res.send(dom.serialize());
});

app.get("/killme", (req, res) => {
  const used = process.memoryUsage();
  const mb = used.heapUsed / 1000000;
  if (mb > 500) {
    process.exit();
  }
  res.send(`live another day... ${mb} MB used so far`);
});

var server = app.listen(8081, function () {
  var host = server.address().address;
  var port = server.address().port;
  console.log("Example app listening at http://%s:%s", host, port);
});
