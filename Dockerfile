FROM node:12.21.0-alpine3.12

COPY ./ ./

RUN npm install
RUN npm install pm2@latest -g
RUN npm link pm2

CMD ["pm2-runtime", "start", "pm2.json"]
