# MathJax Render Server

This server exists as a utility to render LaTex into svg format.

## Installation

Make sure npm is installed. If it is not consider installing nvm to install and manage different versions of node: https://github.com/nvm-sh/nvm. we tested this with node v12

once npm is in installed run `npm install` to make sure all dependencies are installed.

To test if the installation was successful run `npm start`

## running many instances at once with docker swarm

First make sure you have docker installed and swarm enabled.

to check if you have docker installed and that swarm mode is activated run "docker info" and check to see that the swarm field is set to "active"

simply run: docker stack deploy mathjax-render --compose-file docker-compose.yml

to increase the amount of containers running alter the replicas field in the docker-compose file

similarily if port 8125 is taken on your machine change the port specified in the publish field
